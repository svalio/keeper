import React from "react";
import { render } from "react-dom";
import { Route, Switch, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";

import configureStore, { history } from "./configureStore";

const store = configureStore();

const rootElement = document.getElementById("app");

const Root = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        {/* <Route exact path="/" component={} /> 
        <Route path="/login" component={} />
        <Route path="/main/" component={} />
        <Route path="/:call" component={} /> 
        <Redirect to="/" /> */}
      </Switch>
    </ConnectedRouter>
  </Provider>
);

render(<Root />, rootElement);
export default store;
