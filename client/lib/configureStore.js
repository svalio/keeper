// eslint-disable-next-line object-curly-newline
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";
import { connectRouter, routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";
import main from "./main/reducer";

export const history = createBrowserHistory();

const rootReducer = combineReducers({
  router: connectRouter(history)
});

export default function configureStore() {
  const initialState = {};
  const middleware = [routerMiddleware(history), thunk];

  const args = [
    applyMiddleware(...middleware),
    // eslint-disable-next-line operator-linebreak
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  ].filter(Boolean);

  return createStore(rootReducer, initialState, compose(...args));
}
