import axios from "axios";
import localStorage from "./LocalStorageService";

export default {
  // eslint-disable-next-line consistent-return
  async request(args) {
    try {
      const token = localStorage.get("accessToken");
      const headers = token ? { Authorization: `Bearer ${token}` } : {};
      const response = await axios({
        ...args,
        headers,
        baseURL: window.location.origin
      });
      if (response) return response.data;
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(args.errorMsg, e);
      throw new Error(args.errorMsg);
    }
  },

  async post(
    url,
    { body, params },
    errorMsg = "Error. Can't execute post method"
  ) {
    const result = await this.request({
      url,
      data: body,
      params,
      errorMsg,
      method: "post"
    });
    return result;
  },

  async get(
    url,
    { body, params },
    errorMsg = "Error. Can't execute get method"
  ) {
    const result = await this.request({
      url,
      data: body,
      params,
      errorMsg,
      method: "get"
    });
    return result;
  },

  async put(
    url,
    { body, params },
    errorMsg = "Error. Can't execute put method"
  ) {
    const result = await this.request({
      url,
      data: body,
      params,
      errorMsg,
      method: "put"
    });
    return result;
  },

  async delete(
    url,
    { body, params },
    errorMsg = "Error. Can't execute delete method"
  ) {
    const result = await this.request({
      url,
      data: body,
      params,
      errorMsg,
      method: "delete"
    });
    return result;
  }
};
