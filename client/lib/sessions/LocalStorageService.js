export default {
  set(name, value) {
    try {
      if (window.localStorage) {
        localStorage.setItem(name, value);
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
  },

  get(name) {
    try {
      if (window.localStorage) {
        return localStorage.getItem(name);
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
    return null;
  },

  delete(name) {
    try {
      if (window.localStorage) {
        localStorage.removeItem(name);
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
  },

  clear() {
    try {
      if (window.localStorage) {
        localStorage.clear();
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
  }
};
