import React from "react";

export default function() {
  return (
    <nav className="navbar navbar-default">
      <div className="container">
        <div className="navbar-header">
          <a className="navbar-brand" href="/">
            <img
              className="demo-logo"
              src="/images/openvidu_vert_white_bg_trans_cropped.png"
              alt="logo"
            />
            JS Node
          </a>
          <a
            className="navbar-brand nav-icon"
            href="https://github.com/OpenVidu/openvidu-tutorials/tree/master/openvidu-js-node"
            title="GitHub Repository"
            target="_blank"
            rel="noopener noreferrer"
          >
            <i className="fa fa-github" aria-hidden="true" />
          </a>
          <a
            className="navbar-brand nav-icon"
            href="http://www.openvidu.io/docs/tutorials/openvidu-js-node/"
            title="Documentation"
            target="_blank"
            rel="noopener noreferrer"
          >
            <i className="fa fa-book" aria-hidden="true" />
          </a>
        </div>
      </div>
    </nav>
  );
}
