import * as types from "./actionsTypes";

const initialState = {
  flags: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.SHOW_FLAG: {
      const flags = state.flags.concat({
        id: action.id,
        title: action.title,
        description: action.description,
        typeOfFlag: action.typeOfFlag,
        actions: action.actions
      });
      return { ...state, flags };
    }

    case types.DISMISS_FLAG: {
      const flags = state.flags.filter(flag => flag.id !== action.id);
      return { ...state, flags };
    }

    default:
      return state;
  }
}
