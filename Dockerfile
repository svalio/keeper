FROM node:10-alpine as web

WORKDIR /usr/app
COPY . .

WORKDIR /usr/app/client
RUN npm install --quiet
RUN npm run watch

WORKDIR /usr/app/server
RUN npm install --quiet

CMD [ "npm", "start" ]