const { createDatabase } = require("./config");

const database = createDatabase();

async function connect() {
  try {
    await database.authenticate();
    console.log("DATABASE CONNECTED.");
  } catch (err) {
    console.error(err);
  }
}

async function sync() {
  try {
    await database.sync();
    console.log("TABLES CREATED OR RELOADED");
  } catch (err) {
    console.error(err);
  }
}
module.exports = { database, connect, sync };
