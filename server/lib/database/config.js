const Sequelize = require("sequelize");

const config = {
  development: {
    username: "postgres",
    password: "postgres",
    database: "keeper",
    host: "db",
    dialect: "postgres"
  },
  test: {
    username: "postgres",
    password: "postgres",
    database: "keeper",
    host: "db",
    dialect: "postgres"
  },
  production: {
    username: "postgres",
    password: "postgres",
    database: "keeper",
    host: process.env.DATABASE,
    dialect: "postgres"
  }
};

function createDatabase() {
  const environment = process.env.NODE_ENV || "development";
  const options = {
    operatorsAliases: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };

  if (environment === "production") {
    return new Sequelize(process.env.DATABASE_URL, { ...options });
  }

  return new Sequelize({
    ...config[environment],
    ...options
  });
}

module.exports = {
  createDatabase
};
