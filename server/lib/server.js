/*eslint no-sync: ["error", { allowAtRootLevel: true }]*/
/* CONFIGURATION */

const database = require("./database");

// Node imports
const path = require("path");
const express = require("express");
const session = require("express-session");
const http = require("http");
const bodyParser = require("body-parser"); // Pull information from HTML POST (express4)
const app = express(); // Create our app with express

const PORT = 5050;

process.env.NODE_ENV = process.env.NODE_ENV | "development";

database.connect();
database.sync();

// Server configuration

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(
  session({
    saveUninitialized: true,
    resave: false,
    secret: "MY_SECRET"
  })
);

app.use(express.static(path.resolve(__dirname, "../public"))); // Set the static files location

app.use(
  bodyParser.urlencoded({
    extended: "true"
  })
); // Parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // Parse application/json
app.use(
  bodyParser.json({
    type: "application/vnd.api+json"
  })
); // Parse application/vnd.api+json as json

// Listen (start app with node server.js)
try {
  http.createServer(app).listen(PORT);
  console.log(`App listening on port ${PORT}`);
} catch (err) {
  console.error(err);
}
/* CONFIGURATION */

app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "../public/index.html"));
});
